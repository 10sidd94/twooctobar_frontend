import React from 'react';
import { Carousel } from 'react-bootstrap';

// import image1 from './../assets/images/1.jpg';
import image1 from './assets/images/download1.jfif';
import image2 from './assets/images/download2.jfif';
import image3 from './assets/images/download3.jfif';

const CarouselContainer = () => {
  return (
    <Carousel fade={true} pause={false}>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src={image1}
          alt="First slide"
          width="30px" 
          height="500px" 
        />
        <Carousel.Caption>
          <h3>Book Vehicle as per your segment</h3>
          <p>....</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src={image2}
          alt="Third slide"
          width="30px" 
          height="500px" 
        />
        <Carousel.Caption>
          <h3>Now book at lowest cost in market</h3>
          <p>.....</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src={image3}
          alt="Third slide"
          width="30px" 
          height="500px" 
        />
        <Carousel.Caption>
          <h3>vehicle as your choice</h3>
          <p>......</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  )
}

export default CarouselContainer;