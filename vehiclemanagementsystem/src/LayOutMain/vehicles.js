
import React, { Component } from 'react';
import VehicleService  from './VehicleService';
import { Link } from "react-router-dom";
import  {  useEffect } from "react";
import history from './historyInClass'

class ListVehicleComponent extends Component{

    constructor(props) {
        console.log("Vehicle constructor");
       
        super(props)

        this.state = {
            vehicle: []
        }
        
    }
    deleteVehicle(vId){
        VehicleService.deleteVehicle(vId).then( res => {
        this.setState({vehicle: this.state.vehicle.filter(vehicle=> vehicle.vId !== vId)});
        });
    }
    logout(){
        console.log("logout" );
        history.push("/logout");
        window.location.reload(true);
        
    }

  
    componentDidMount(){
        VehicleService.getVehicles().then((res) => {
            this.setState({ vehicle : res.data});
            console.log(res.data)
        });
    }

   
   render() {
        return (
            <div>
                 <h2 className="text-center">Vehicle List</h2>
                 <div className = "row">
                    <Link class="btn btn-outline-primary mr-2" to={`/vehicle/addVehicle`} >Add Vehicle </Link>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Vehicle Id</th>
                                    <th> Vehicle type</th>
                                    <th> Vehicle chessisNumber</th>
                                    <th> Vehicle Model Year</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.vehicle.map(
                                        vehicle => 
                                        
                                        <tr key = {vehicle.vId}>
                                            <td>{vehicle.vId}</td>
                                              <td> {vehicle.type} </td>   
                                            <td> {vehicle.chessisNumber}</td>
                                            <td> {vehicle.modelYear}</td>
                                             <td>
                                                 {/* <button onClick={ () => this.editUser(user.email)} className="btn btn-info">Update </button> */}
                                                 <Link class="btn btn-outline-primary mr-2" to={`/vehicle/edit/${vehicle.vId}`} >  Edit </Link>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteVehicle(vehicle.vId)} className="btn btn-danger">Delete </button>
                                                 {/* <Link class="btn btn-primary mr-2" to={`/user/${user.email}`}>View </Link> */}
                                                 <Link className="btn btn-primary" to={`/vehicle/${vehicle.vId}`}>View</Link>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>
                 <div>
                 <button onClick={ () => this.logout()}  className="btn btn-primary btn-block"> Logout </button> 
                 </div>
                 
            </div>
        )
    }

    
}

export default ListVehicleComponent;