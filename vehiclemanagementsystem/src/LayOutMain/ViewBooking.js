
import React, { Component } from 'react'
import BookingService from './VehicleBookingService'
import {withRouter} from 'react-router'

class ViewBookingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            //id:1,
            bookingId: this.props.match.params.bookingId,
            booking:{}
        }
    }

    componentDidMount(){
         console.log(this.state.bookingId)
         BookingService.getVehicleBookingById(this.state.bookingId).then( res => {
            console.log(res.data)
            this.setState({booking: res.data});
            console.log(this.booking)
        })
    }

    render() {
        return (
            <div >
                <br></br>
                <div className = "card col-md-6 offset-md-3" >
                    <h3 className = "text-center" > View Vehicle Booking :: Details</h3>
                    <div className = "card-body" >
                        <div className = "row">
                            <label> Vehicle Booking  Id : </label>
                            <div> { this.state.booking.bookingId }</div>
                        </div>
                        <div className = "row">
                            <label> from place : </label>
                            <div> { this.state.booking.fromPlace}</div>
                        </div>
                        <div className = "row">
                            <label> To place </label>
                            <div> { this.state.booking.toPlace}</div>
                        </div>                    </div>

                </div>
            </div>
        )
    }
}

export default withRouter(ViewBookingComponent)


// import React, { useState, useEffect } from "react";
// import { Link, useParams } from "react-router-dom";
// import axios from "axios";

// const viewUser = () => {
//   const [user, setUser] = useState({
//     email: "",
//     password: "",
//     contactnumber: ""
   
//   });

//   const { email } = useParams();



//   useEffect(() => {
//     loadUser();
//   }, []);


//   const loadUser =  () => {
//     console.log("herer : ")
//     const res = axios.get(`http://localhost:9999/api/users/${email}`);
//     console.log(res.data)
//     setUser(res.data);
//   };
//   return (
//     <div className="container py-4">
//       <Link className="btn btn-primary" to="/">
//         back to Home
//       </Link>
//       <h1 className="display-4">User Id: {user.email}</h1>
//       <hr />
//       <ul className="list-group w-50">
//         <li className="list-group-item">user email: {user.email}</li>
//         <li className="list-group-item">user password: {user.password}</li>
//         <li className="list-group-item">user contactnumber: {user.contactnumber}</li>
//       </ul>
//     </div>
//   );
// };

// export default viewUser;

// import React, { useState, useEffect } from "react";
// import { Link, useParams } from "react-router-dom";
// import axios from "axios";

// const User = () => {
//   const [user, setUser] = useState({
//     email: "",
//     password: "",
//     contactnumber: ""
   
//   });

//   const { email } = useParams();



//   useEffect(() => {
//     loadUser();
//   }, []);


//   const loadUser =  () => {
//     console.log("herer : ")
//     const res = axios.get(`http://localhost:9999/api/users/${email}`);
//     console.log(res.data)
//     setUser(res.data);
//   };
//   return (
//     <div className="container py-4">
//       <Link className="btn btn-primary" to="/">
//         back to Home
//       </Link>
//       <h1 className="display-4">User Id: {user.email}</h1>
//       <hr />
//       <ul className="list-group w-50">
//         <li className="list-group-item">user email: {user.email}</li>
//         <li className="list-group-item">user password: {user.password}</li>
//         <li className="list-group-item">user contactnumber: {user.contactnumber}</li>
//       </ul>
//     </div>
//   );
// };

// export default User;


// // import React, { Component } from 'react'
// // import UserService from './UserService'

// // import {withRouter} from 'react-router'

// // class ViewUser extends Component {
// //     constructor(props) {
// //         super(props)

// //         this.state = {
// //             //id:1,
// //             email: this.props.match.params.email,
// //            // email:"10sid@gmail.com",
// //             user: {}
// //         }
// //     }

// //     componentDidMount(){
// //         UserService.getUserById(this.state.email).then( res => {
// //             this.setState({user: res.data});
// //         })
// //     }

// //     render() {
// //         return (
// //             <div>
// //                 <br></br>
// //                 <div className = "card col-md-6 offset-md-3">
// //                     <h3 className = "text-center"> View User Details</h3>
// //                     <div className = "card-body">
// //                         <div className = "row">
// //                             <label> User Email: </label>
// //                             <div> { this.state.user.email }</div>
// //                         </div>
// //                         <div className = "row">
// //                             <label> User password: </label>
// //                             <div> { this.state.user.password}</div>
// //                         </div>
// //                         <div className = "row">
// //                             <label> User contact Number: </label>
// //                             <div> { this.state.user.contactnumber}</div>
// //                         </div>
// //                     </div>

// //                 </div>
// //             </div>
// //         )
// //     }
// // }


// // export default withRouter(ViewUser)