
import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";

const UpdateVehicle = () => {

    let history = useHistory();
    const { vId } = useParams();

    const [vehicle, setVehicle] = useState({
        chessisNumber: "",
        modelYear: ""
    });

    const { chessisNumber, modelYear } = vehicle;


    const onInputChange = e => {

        setVehicle({ ...vehicle, modelYear: e.target.value });
        console.log("on change : " + vehicle.modelYear)
    };

    useEffect(() => {
        loadVehicle();
    }, []);

    const onSubmit = async () => {
        console.log("on submit" + vehicle.modelYear)
        await axios.put(`http://localhost:8080/spring_backend/vehicle/${vId}`, vehicle);
        history.push("/Vehicles");
    };

    const loadVehicle = async () => {
        console.log(vId)
        const result = await axios.get(`http://localhost:8080/spring_backend/vehicle/${vId}`);
        setVehicle({ vehicle: result.data });
        console.log({ vehicle })
    };
    return (
        <div className="container">
            <div className="w-75 mx-auto shadow p-5">
                <h2 className="text-center mb-4">Edit Vehicle details </h2>
                <form onSubmit={e => onSubmit(e)}>

                    <div className="form-group">
                        <input


                            type="number"
                            className="form-control form-control-lg"
                            placeholder="User Id"
                            name="vId"
                            value={vId}
                            onChange={e => onInputChange(e)}
                        />
                    </div>


                    <div className="form-group">
                        <input

                            type="String" name="password"

                            className="form-control form-control-lg"
                            placeholder="not  updatatble"
                            name="chessisNumber"
                            value={chessisNumber}
                            onChange={e => onInputChange(e)}
                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="number"
                            className="form-control form-control-lg"
                            // placeholder=" Only ModelYear is not  updatatble"
                            name="modelYear"
                            value={modelYear}
                            onChange={e => onInputChange(e)}
                        />
                    </div>


                    <button className="btn btn-warning btn-block">Update Vehicle</button>
                </form>
            </div>
        </div>
    );
};

export default UpdateVehicle;




//make this component using function

//it will work
// import React, { Component } from 'react'
// import UserService from './UserService'
// import { withRouter } from 'react-router'
// import axios from 'axios'

// class UpdateUser extends Component {

//     constructor(props) {
//         super(props)

//         this.state = {
//             //id:1,
//             email: this.props.match.params.email,
//             user: {},
//             password:"",
//             contactnumber:""


//         }


//     }



//     componentDidMount() {
//         console.log(this.state.email)
//         UserService.getUserById(this.state.email).then(res => {
//             console.log(res.data)
//             this.setState({ user: res.data[0] });
//             this.password=res.data[0].password
//             this.contactnumber=res.data[0].contactnumber
//             console.log(this.password)
//             console.log(this.contactnumber)
//         })
//     }


//     // onInputChange = e => {
//     //     this.setState({ [e.target.password]: e.target.value });
//     // };


//     // onSubmit = async e => {
//     //     e.preventDefault();
//     //     await axios.put(`http://localhost:9999/api/users/${this.state.email}`, this.user);
//     //     // history.push("/Users");
//     //     console.log("kuch toh ua")
//     // };

//     render() {
//         return (
//             <div className="container">
//                 <div className="w-75 mx-auto shadow p-5">
//                     <h2 className="text-center mb-4">Edit A User</h2>
//                     <form onSubmit={ async e =>  await axios.put(`http://localhost:9999/api/users/${this.state.email}`, this.user)}>
//                         <form>
//                             <div className="form-group">
//                                 <input
//                                     type="email"
//                                     className="form-control form-control-lg"
//                                     placeholder="Enter Your Email"
//                                     name="email"
//                                     value={this.state.user.email}
//                                 //   onChange={e => onInputChange(e)}
//                                 />
//                             </div>
//                             <div className="form-group">
//                                 <input
//                                     type="password"
//                                     className="form-control form-control-lg"
//                                     placeholder="Enter Your password"
//                                     name="password"
//                                     value={this.state.user.password}
//                                     // onChange={e => onInputChange(e)}
//                                 />
//                             </div>
//                             <div className="form-group">
//                                 <input
//                                     type="number"
//                                     className="form-control form-control-lg"
//                                     placeholder="Enter Your Contact Numbers"
//                                     name="contactnumber"
//                                     value={this.contactnumber}
//                                     onChange={e => this.setState({ [e.target.password]: e.target.value })}
//                                 />
//                             </div>

//                             <button className="btn btn-warning btn-block">Update User</button>
//                         </form>
//                         </form>
//       </div>
//                 </div>
//         )}
// }
//                 export default withRouter(UpdateUser)