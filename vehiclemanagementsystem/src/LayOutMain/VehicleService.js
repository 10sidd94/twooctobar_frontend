import axios from 'axios'

const USERS_API_BASE_URL="http://localhost:8080/spring_backend/vehicle"


class VehicleService
{

    
    getVehicles()
    {
        return axios.get(USERS_API_BASE_URL+'/vList')
    }

    getVehicleById(vId)
    {
        return axios.get(USERS_API_BASE_URL+'/'+vId)
    }
    
    deleteVehicle(vId)
    {
        return axios.delete(USERS_API_BASE_URL+'/'+vId)
    }

    updateUser(email)
    {
        return axios.put(USERS_API_BASE_URL+'/'+email)
    }
}

export default new VehicleService()