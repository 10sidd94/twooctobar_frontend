import axios from 'axios'

const USERS_API_BASE_URL = "http://localhost:8080/spring_backend/vehicleUsage"

class UsageService {


    getUsage() {
        return axios.get(USERS_API_BASE_URL + '/usList')
    }

    getUsageById(usageId) {
        return axios.get(USERS_API_BASE_URL + '/' + usageId)
    }

    deleteUsageById(usageId) {
        return axios.delete(USERS_API_BASE_URL + '/' + usageId)
    }

    updateUsage(usageId) {
        return axios.put(USERS_API_BASE_URL + '/' + usageId)
    }
}

export default new UsageService()