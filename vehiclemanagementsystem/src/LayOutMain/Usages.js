
import React, { Component } from 'react';
import UsageService  from './UsageService';
import { Link } from "react-router-dom";
import  {  useEffect } from "react";
import history from './historyInClass'

class ListUsagesComponent extends Component{

    constructor(props) {
        console.log("Usage constructor");
        super(props)

        this.state = {
            usage: []
        }

    }
    deleteUsage(usageId){
        UsageService.deleteUsageById(usageId).then( res => {
        this.setState({usage: this.state.usage.filter(usage=> usage.usageId !== usageId)});
        });
    }
  

    logout(){
        console.log("logout" );
        history.push("/logout");
        window.location.reload(true);
        
    }


    componentDidMount(){
        UsageService.getUsage().then((res) => {
            this.setState({ usage: res.data});
            console.log(res.data)
        });
    }

   
   render() {
        return (
            <div>
                 <h2 className="text-center"> Vehicle Usage List</h2>
               
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Usage Id </th>
                                    <th> unitCharge</th>
                                    <th> unit of Usage</th>
                                    <th> Total </th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.usage.map(
                                        usage => 
                                        <tr key = {usage.usageId}>
                                             <td> {usage.usageId} </td> 
                                             <td> {usage.unitCharge} </td>   
                                             <td> {usage.usageUnits}</td>
                                             <td> {usage.total} </td> 
                                             <td>
                                                 <Link class="btn btn-outline-primary mr-2" to={`/vehicleUsage/edit/${usage.usageId}`} >  Edit </Link>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteUsage(usage.usageId)} className="btn btn-danger">Delete </button>
                                                 <Link className="btn btn-primary" to={`/vehicleUsage/${usage.usageId}`}>View</Link>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>
                 <div>
                 <button onClick={ () => this.logout()}  className="btn btn-primary btn-block"> Logout </button> 
                 </div>
                 
            </div>
        )
    }

    
}

export default ListUsagesComponent;