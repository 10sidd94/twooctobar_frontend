import React, { useState } from "react";
import axios from 'axios'
import { useHistory } from "react-router-dom";

const AddVehicle = () => {
  let history = useHistory();
  const [vehicle, setVehicle] = useState({
 "chessisNumber":"",
 "modelYear":"",



  });

  const {chessisNumber,modelYear} = vehicle;
  const onInputChange = e => {
    setVehicle({ ...vehicle,[e.target.name]:e.target.value });
  };

  const onSubmit = async e => {
    e.preventDefault();
    console.log(vehicle)
    await axios.post("http://localhost:8080/spring_backend/vehicle/addVehicle",vehicle);
    history.push("/Vehicles");
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Add Vehicle</h2>
        <form onSubmit={e => onSubmit(e)}>

        <div className="form-group">
            <input
              type="chessisNumber"
              className="form-control form-control-lg"
              placeholder="Enter chessisNumber"
              name="chessisNumber"
              
              onChange={e => onInputChange(e)}
            />
          </div>


          <div className="form-group">
            <input
              type="modelYear"
              className="form-control form-control-lg"
              placeholder="Enter Model year of vehicle"
              name="modelYear"
              
              onChange={e => onInputChange(e)}
            />
          </div>
          {/* <div className="form-group">
            <input
              type="number"
              className="form-control form-control-lg"
              placeholder="Enter Your contact number"
              name="contactNumber"
              onChange={e => onInputChange(e)}
            />
          </div> */}
          

         
          

        
          <button className="btn btn-primary btn-block">Add Vehicle</button>
        </form>
      </div>
    </div>
  );
};

export default AddVehicle