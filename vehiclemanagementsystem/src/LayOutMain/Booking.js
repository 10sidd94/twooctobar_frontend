
import React, { Component } from 'react';
import BookingService from './VehicleBookingService';
import { Link } from "react-router-dom";
import EditBooking from './EditBooking';
import history from './historyInClass'

class ListBookingComponent extends Component {

    constructor(props) {
        console.log("User constructor");
        super(props)

        this.state = {
            booking: []
        }

    }

    deleteBooking(bookingId){
        BookingService.deleteVehicleBookingById(bookingId).then( res => {
        this.setState({booking: this.state.booking.filter(booking=> booking.bookingId !== bookingId)});
        });
    }

    componentDidMount() {
        BookingService.getVehicleBooking().then((res) => {
            this.setState({ booking: res.data });
            console.log(res.data)
        });
    }

    logout(){
        console.log("logout" );
        history.push("/logout");
        window.location.reload(true);
        
    }

    render() {
        return (
            <div>
                <h2 className="text-center">All Booking List</h2>

                <br></br>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th> Booking   Id</th>
                                <th> from place of Boking</th>
                                <th> to place of booking</th>
                                <th> Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.booking.map(
                                    booking =>
                                        <tr key={booking.bookingId}>
                                            <td> {booking.bookingId} </td>
                                            <td> {booking.fromPlace} </td>
                                            <td> {booking.toPlace}</td>
                                            <td>
                                                <Link class="btn btn-outline-primary mr-2" to={`/vehicleBooking/edit/${booking.bookingId}`} >  Edit </Link>
                                                <button style={{ marginLeft: "10px" }} onClick={() => this.deleteBooking(booking.bookingId)} className="btn btn-danger">Delete </button>
                                                <Link className="btn btn-primary" to={`/vehicleBooking/${booking.bookingId}`}>View</Link>
                                            </td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
                <div>
                 <button onClick={ () => this.logout()}  className="btn btn-primary btn-block"> Logout </button> 
                 </div>
            </div>
        )
    }


}

export default ListBookingComponent;