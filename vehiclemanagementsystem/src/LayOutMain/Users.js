
import React, { Component } from 'react';
import UserService  from './UserService';
import { Link, useHistory } from "react-router-dom";
import  {  useEffect } from "react";
import history from './historyInClass'

class ListUserComponent extends Component{
    
    constructor(props) {
        console.log("User constructor");
        super(props)
        
        this.state = {
            user: [],
            
        }

    }
//change delete with glyficon or google icon
    deleteUser(userId){
        UserService.deleteUserById(userId).then( res => {
        this.setState({user: this.state.user.filter(user=> user.userId !== userId)});
        });
    }
    

    componentDidMount(){
        UserService.getUsers().then((res) => {
            this.setState({ user: res.data});
            console.log(res.data)
        });
    }

    logout(){
        console.log("logout" );
        history.push("/logout");
        window.location.reload(true);
        
    }
   render() {
        return (
            <div>
                 <h2 className="text-center">User List</h2>
                 <div className = "row">
                    <Link class="btn btn-outline-primary mr-2" to={`/user/adduser`} >Add User </Link>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> User  Id</th>
                                    <th> User Email Id</th>
                                    <th> User Password</th>
                                    <th> User Contact Number</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.user.map(
                                        user => 
                                        <tr key = {user.email}>
                                             <td> {user.userId} </td> 
                                             <td> {user.email} </td>   
                                             <td> {user.password}</td>
                                             <td> {user.contactNumber}</td>
                                             <td>
                                                 <Link class="btn btn-outline-primary mr-2" to={`/user/edit/${user.userId}`} >  Edit </Link>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteUser(user.userId)} className="btn btn-danger">Delete </button>
                                                 <Link className="btn btn-primary" to={`/user/${user.userId}`}>View</Link>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>
                 <div>
                 <button onClick={ () => this.logout()}  className="btn btn-primary btn-block"> Logout </button> 
                 </div>
                 
            </div>
        )
    }

    
}

export default ListUserComponent;