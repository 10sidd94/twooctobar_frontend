import CarouselContainer from '../CarouselContainer';

function App() {
  return (
    <div className="App">
      <CarouselContainer />
    </div>
  );
}

export default App;